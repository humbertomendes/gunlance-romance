const express = require('express');
const shortid = require('shortid');

const router = express.Router();

// Require Item model in our routes module
var Loadout = require('../models/Loadout');

const node = "/loadouts";

// Defined get data(index or listing) route
router.get("/", function (request, response) {
    Loadout.find(function (err, loadouts){
        if(err){
            console.log(err);
        }
        else {
            response.status(200).json(loadouts);
        }
    });
});

// Load Loadout by UID
router.get("/:uid", function(request, response) {
    var uid = request.params.uid;
    Loadout.findOne({"uid":uid}, function (err, item){
        if(item){
            response.status(200).json(item);
        }else{
            response.status(400).json({"error":"Invalid Loadout"});
        }
    });
});

// Defined store route
router.post("/", function (request, response) {
    var loadout = new Loadout(request.body);
    loadout.uid = shortid.generate();
    loadout.created = new Date().toISOString();
    loadout.save()
        .then(loadout => {
            response.status(200).json({"uid": loadout.uid});
        })
        .catch(err => {
            response.status(400).json(err);
        });
});

/*
// Defined edit route
router.put("/:uid", function (request, response) {
    var uid = request.params.uid;
    Loadout.findById(uid, function (err, item){
        response.json(item);
    });
});
*/

//  Defined update route
router.patch("/:uid", function (request, response) {
    Loadout.findById(request.params.uid, function(err, item) {
        if (!item)
            return next(new Error('Could not load Document'));
        else {
            Loadout.name = request.body.name;
            Loadout.price = request.body.price;

            Loadout.save().then(item => {
                response.json('Update complete');
            })
            .catch(err => {
                response.status(400).send("unable to update the database");
            });
        }
    });
});

// Defined delete | remove | destroy route
router.delete("/:uid", function (request, response) {
    Loadout.findByIdAndRemove({_id: request.params.uid}, function(err, item){
        if(err) response.json(err);
        else response.json('Successfully removed');
    });
});

module.exports = router;