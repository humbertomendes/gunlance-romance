const express = require('express');
const NodeCache = require("node-cache");    

const dataApi = require("../data/api");
const dataCache = require("../data/cache");

const router = express.Router();
const cache = new NodeCache({ stdTTL: 60, checkperiod: 120 });

const node = "/armor";

router.get("/id/:id", function(request, response) {
    let id = request.params.id;
    let key = dataCache.makeKey(id);

    let config = {
        callback: function(data, noCache) {
            // If data is valid
            if(config.validate(data)){
                if(!noCache) cache.set(key, data);
            }            
            response.status(200).send(data);
        },
        cache: {
            ref: cache,
            key: key,
            value: id,
            prop: "id"
        },
        api: {
            path: `${node}/${id}`
        },
        validate: dataApi.validateData
    };
    
    getData(config);
});

router.get("/:type/:query/:name", function(request, response) {
    let type = request.params.type;
    let query = request.params.query;
    let name = encodeURIComponent(request.params.name);
    let key = dataCache.makeKey(name, `${type}-${query}`);

    let config = {
        callback: function(data, noCache) {
            // If data is valid
            if(config.validate(data)) {
                if(!noCache) cache.set(key, data);               
            } else {
                data = [{ id: -1, name: "No results" }];
            }
            response.status(200).send(data);
        },
        cache: {
            ref: cache,
            key: key,
            value: name,
            prop: "name",
            filter: dataCache.filterByValue
        },
        api: {
            path: `${node}?q={"rank":"high","type":"${type}","${query}":{"$like":"${name}%"}}`
        },
        validate: dataApi.validateArrayData
    };

    getData(config);
});

function getData(config) {
    var data = dataCache.getData(config.cache);
    if (data) {
        config.callback(data, false);;
    } else {
        dataApi.getData(config.api, config.callback);
    }
}

module.exports = router;