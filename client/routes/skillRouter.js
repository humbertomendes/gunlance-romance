const express = require('express');
const NodeCache = require( "node-cache" );

const dataApi = require("../data/api");
const dataCache = require("../data/cache");

const router = express.Router();
const cache = new NodeCache({ stdTTL: 600, checkperiod: 1200 });

const node = "/skills";

router.get("/:id", function(request, response) {
    let id = parseInt(request.params.id);
    let key = id;
    
    let config = {
        callback: function(data, noCache) {            
            // If data is valid
            if(config.validate(data)) {
                if(!noCache) cache.set(key, data);
            } else {
                data = [{ id: -1, name: "No results" }];
            }
            response.status(200).send(data);
        },
        cache: {
            ref: cache,
            value: id,
            prop: "id",
            key: key
        },
        api: {
            path: `${node}/${id}`
        },
        validate: dataApi.validateData
    };
    
    getData(config);
});

function getData(config,) {
    var data = dataCache.getData(config.cache);
    if (data) {
        config.callback(data, false);
    } else {
        dataApi.getData(config.api, config.callback);
    }
}

module.exports = router;