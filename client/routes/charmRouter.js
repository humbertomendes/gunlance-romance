const express = require('express');
const NodeCache = require("node-cache");    

const dataApi = require("../data/api");
const dataCache = require("../data/cache");

const router = express.Router();
const cache = new NodeCache({ stdTTL: 600, checkperiod: 1200 });

const node = "/charms";

router.get("/id/:id/rank/:rank", function(request, response) {
    let id = request.params.id;
    let rank = request.params.rank;
    let key = dataCache.makeKey(id);

    let config = {
        callback: function(data, noCache) {
            // If data is valid
            let charm = null;
            if(config.validate(data)) {
                if(!noCache) cache.set(key, data);
                charm = formatCharm(data.id, data.ranks[rank-1]);
            }
            response.status(200).send(charm);
        },
        cache: {
            ref: cache,
            key: key,
            value: id,
            prop: "id"
        },
        api: {
            path: `${node}/${id}`
        },
        validate: dataApi.validateData
    };
    
    getData(config);
});

router.get("/:query/:name", function(request, response) {
    let query = request.params.query;
    let name = encodeURIComponent(request.params.name);
    let key = dataCache.makeKey(name, query);
    
    let config = {
        callback: function(data, noCache) {
            // If data is valid
            if(config.validate(data)) {
                if(!noCache) cache.set(key, data);

                let charms = [];
                let charm = null;
                let rank = null;
                
                for(var i = 0; i < data.length; i++) {
                    charm = data[i];
                    for(var k = 0; k < charm.ranks.length; k++) {
                        charms.push(formatCharm(charm.id, charm.ranks[k]));
                    }
                }
                data = charms;
            } else {
                data = [{ id: -1, name: "No results" }];
            }
            response.status(200).send(data);
        },
        cache: {
            ref: cache,
            value: name,
            prop: "name",
            key: key,
            filter: dataCache.filterByValue
        },
        api: {
            path: `${node}?q={"${query}":{"$like":"${name}%"}}`
        },
        validate: (data) => dataApi.validateArrayData
    };

    getData(config);
});

function getData(config) {
    var data = dataCache.getData(config.cache);
    if (data) {
        config.callback(data, false);
    } else {
        dataApi.getData(config.api, config.callback);
    }
}

function formatCharm(id, rank){
    return {
        id: id,
        rank: rank.level,
        name: rank.name,
        rarity: rank.rarity,
        skills: rank.skills,
        crafting: rank.crafting
    }
}

module.exports = router;