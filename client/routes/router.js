const express = require('express');
const router = express.Router();

const loadoutRouter = require('./loadoutRouter');
const armorRouter = require('./armorRouter');
const charmRouter = require('./charmRouter');
const decorationRouter = require('./decorationRouter');
const skillRouter = require('./skillRouter');
const weaponRouter = require('./weaponRouter');

router
    // Add a binding to handle '/test'
    //.get('/', function(){
    // render the /tests view
    //})

    // Import my automated routes into the path '/tests/automated'
    // This works because we're already within the '/tests' route so we're simply appending more routes to the '/tests' endpoint
    .use('/loadouts', loadoutRouter)
    .use('/armors', armorRouter)
    .use('/charms', charmRouter)
    .use('/decorations', decorationRouter)
    .use('/skills', skillRouter)
    .use('/weapons', weaponRouter);

module.exports = router;