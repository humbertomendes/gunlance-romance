const express = require('express');
const NodeCache = require("node-cache");    

const dataApi = require("../data/api");
const dataCache = require("../data/cache");

const router = express.Router();
const cache = new NodeCache({ stdTTL: 600, checkperiod: 1200 });

const node = "/decorations";

router.get("/id/:id", function(request, response) {
    let id = request.params.id;
    let key = dataCache.makeKey(id);

    let config = {
        callback: function(data, noCache) {
            // If data is valid
            if(config.validate(data)){
                if(!noCache) cache.set(key, data);
            }            
            response.status(200).send(data);
        },
        cache: {
            ref: cache,
            key: key,
            value: id,
            prop: "id"
        },
        api: {
            path: `${node}/${id}`
        },
        validate: dataApi.validateData
    };
    
    getData(config);
});

router.get("/:query/:name/rank/:rank", function(request, response) {
    let query = encodeURIComponent(request.params.query);
    let name = encodeURIComponent(request.params.name);
    let rank = request.params.rank;
    
    /*
    cache: {
    skip: true,
    ref: decorationCache,
    value: name,
    prop: "name",
    filter: filterByValue,
    key: makeKey(name)
    },
    */

    let config = {
        callback: function(data){            
            // If data is valid
            if(!config.validate(data)) {
                data = [{ id: -1, name: "No results" }];
            }
            response.status(200).send(data);
        },
        api: {
            path: `${node}?q={"${query}":{"$like":"${name}%"},"slot":{"$lte":${rank}}}`
        },
        validate: dataApi.validateArrayData
    };

    dataApi.getData(config.api, config.callback);
});

function getData(config) {
    var data = dataCache.getData(config.cache);
    if (data) {
        config.callback(data, false);
    } else {
        dataApi.getData(config.api, config.callback);
    }
}

module.exports = router;