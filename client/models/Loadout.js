var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Define collection and schema for Items
var Loadout = new Schema(
    {
        uid: {
            type: String,
            required: true
        },
        weapon: {
            id:{
                type: Number,
                required: true
            },
            type: {
                type: String,
                required: true
            },
            decoIds: Schema.Types.Mixed
        },
        head: {
            id:{
                type: Number,
                required: true
            },
            decoIds: Schema.Types.Mixed
        },
        chest: {
            id:{
                type: Number,
                required: true
            },
            decoIds: Schema.Types.Mixed
        },
        gloves: {
            id:{
                type: Number,
                required: true
            },
            decoIds: Schema.Types.Mixed
        },
        waist: {
            id:{
                type: Number,
                required: true
            },
            decoIds: Schema.Types.Mixed
        },
        legs: {
            id:{
                type: Number,
                required: true
            },
            decoIds: Schema.Types.Mixed
        },
        charm: {
            id:{
                type: Number,
                required: true
            },
            rank:{
                type: Number
            }
        },
        created: {
            type: Date,
            required: true
        },
        updated: {
            type: Date
        }
    },
    {
        collection: 'loadouts'
    }
);

module.exports = mongoose.model('Loadout', Loadout);