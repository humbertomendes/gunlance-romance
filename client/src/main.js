import Vue from 'vue';
import VueRouter from 'vue-router';
import BootstrapVue from 'bootstrap-vue';

import App from './App.vue';
import Loadout from './Loadout.vue';
import LoadoutList from './LoadoutList.vue';
import ProductList from './ProductList.vue';
import Product from './Product.vue';

// Import Bootstrap CSS
import 'bootstrap-vue/dist/bootstrap-vue.css';
import 'bootstrap/dist/css/bootstrap.css';

Vue.use(VueRouter);
Vue.use(BootstrapVue);

const routes = [
  {
    name: 'index',
    path: '/',
    redirect: '/loadouts'
  },
  {
    name: 'loadouts',
    path: '/loadouts/list',
    component: LoadoutList
  },
  {
    name: 'new-loadout',
    path: '/loadouts',
    component: Loadout
  },
  {
    name: 'load-loadout',
    path: '/loadouts/:uid',
    component: Loadout
  }
];

 // Just routes is a shorthand for routes: routes
const router = new VueRouter({
  routes
});

Vue.prototype.$apiDomain = process.env.NODE_ENV === "development" ? "http://localhost" : "";
new Vue({
  el: '#app',
  router,
  render: h => h(App)
});