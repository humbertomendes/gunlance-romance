const express = require('express');
const bodyParser = require("body-parser");
const history = require('connect-history-api-fallback');
const app = express();
const mongoose = require('mongoose');
const config = require('./config/DB');

mongoose.Promise = global.Promise;
mongoose.connect(config.DB).then(
    () => {console.log('Database is connected') },
    err => { console.log('Can not connect to the database'+ err)}
);

const staticFileMiddleware = express.static(__dirname);
app.use(staticFileMiddleware);
/*
app.use(history({
  index: "/",
  disableDotRule: true,
  verbose: true,
  rewrites: [
    {
      from: /\/api/,
      to: function(context) {
        return context.parsedUrl.pathname;
      }
    }
  ]
}));
*/
if(process.env.NODE_ENV === "development"){
  app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });
}

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const port = process.env.PORT || 80;
app.listen(port, () => {
  console.log(`Example app listening on port ${port}!`);
});

// Internal
const router = require("./routes/router");
// Import my test routes into the path '/test'
app.use('/api', router);

// Internal
//const router = require("./routes/router.js");
//router(app);